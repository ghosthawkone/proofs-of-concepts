#include <Windows.h>

LRESULT CALLBACK WindowProc(HWND windowHandle, 
	UINT uMessage, 
	WPARAM params, 
	LPARAM lparam) {
	switch (uMessage)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
		//break;
	case WM_PAINT:
		PAINTSTRUCT paintStruct;
		HDC hdcInstance = BeginPaint(windowHandle, &paintStruct);
		FillRect(hdcInstance, &paintStruct.rcPaint, (HBRUSH)(COLOR_WINDOW + 1));
		EndPaint(windowHandle, &paintStruct);
		return 0;
		//break;
	}
	return 0;
}

int wWinMain(HINSTANCE instanceCurrent, 
	HINSTANCE instancePrev, 
	PWSTR pCommandLine, 
	int cmdShow) {
	LPCSTR class_name = "Hello world!";
	WNDCLASS windowClass = {};
	windowClass.lpfnWndProc = WindowProc;
	windowClass.hInstance = instanceCurrent;
	windowClass.lpszClassName = class_name;
	RegisterClass(&windowClass);
	HWND windowHandle = CreateWindowEx(0,
		class_name,
		"Windows Programming with C++",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		instanceCurrent,
		NULL);
	if (windowHandle == 0)
		return -1;
	ShowWindow(windowHandle, cmdShow);
	MSG message = {};
	while (GetMessage(&message, windowHandle, 0, 0)) {
		TranslateMessage(&message);
		DispatchMessage(&message);
	}
	return 0;
}