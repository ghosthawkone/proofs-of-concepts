﻿using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PoCAspFileUpload
{
    public partial class FileUploadBox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e) {

        }

        public void OnButtonUploadFile(object sender, EventArgs e) {
            ctlFileUploadDialogBox.AllowMultiple = true;
            if (ctlFileUploadDialogBox.HasFile) {
                string filename = Path.GetFileName(ctlFileUploadDialogBox.FileName);
                foreach (var postedFile in ctlFileUploadDialogBox.PostedFiles) {
                    postedFile.SaveAs(Server.MapPath("~/UploadedFiles/") + postedFile.FileName);
                }
            }
        }
    }
}