﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileUploadBox.aspx.cs" Inherits="PoCAspFileUpload.FileUploadBox" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ASP File Upload POC</title>
    <style>
        body {
            background-color:lightsteelblue;
            font-size:large;
        }
        table{
            position:fixed;
            top:50%;
            left:50%;
            transform:translate(-50%,-50%);
        }
        .button_upload_file{
            height:40px;
            width:180px;
            background-color:forestgreen;
            border-color:forestgreen;
            border-style:solid;
            color:white;
            font-size:20px;
        }
        .control_upload_file{
            height:30px;
            width:370px;
            background-color:cadetblue;
            border-color:cadetblue;
            border-style:solid;
        }

        .button_upload_file:hover{
            background-color:navy;
            border-color:navy;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div runat="server"> 
        <table>
            <tr>
                <td></td>
                <td>
                    <asp:FileUpload ID="ctlFileUploadDialogBox" runat="server" CssClass="control_upload_file"/>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Button ID="btnFileUpload" runat="server" Text="Upload file" CssClass="button_upload_file" OnClick="OnButtonUploadFile"/>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
